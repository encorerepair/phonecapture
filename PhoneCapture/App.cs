﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Exceptions;
using Common.Services;
using CustomControls;

namespace PhoneCapture
{
    public partial class App : Form
    {
        public List<Color> Colors { get; set; }
        public List<ToggleSimpleButton> Buttons { get; set; }
        private DeviceInfo _currentDeviceInfo;
        private readonly List<DeviceInfo> _currentDevices;
        private bool _reading;
        private bool Reading
        {
            get => _reading;
            set
            {
                _reading = value;
                SetWaitingState(value);
            }
        }
        private static string _filePath;
        private readonly AppleDeviceInteracter _reader;

        public App()
        {
            InitializeComponent();
            _reader = new AppleDeviceInteracter();
            Colors = new List<Color>
            {
                new Color
                {
                    BtnColor = System.Drawing.Color.FromArgb(255, 242, 244, 243),
                    Name = "Silver",
                    Code = "SLV"
                },
                new Color
                {
                    BtnColor = System.Drawing.Color.FromArgb(255, 237, 217, 193),
                    Name = "Gold",
                    Code = "GLD"
                },
                new Color
                {
                    BtnColor = System.Drawing.Color.FromArgb(255, 203, 204, 209),
                    Name = "Gray",
                    Code = "GRY"
                },
                new Color
                {
                    BtnColor = System.Drawing.Color.FromArgb(255, 255, 0 ,255),
                    Name = "Rose G.",
                    Code = "RGD"
                },
                new Color
                {
                    BtnColor = System.Drawing.Color.FromArgb(255, 0, 0, 0),
                    Name = "Jet B.",
                    Code = "JBLK"
                },
                //new Color
                //{
                //    BtnColor = System.Drawing.Color.FromArgb(255, 255, 0 ,255),
                //    Name = "Silver",
                //    Code = "SLV"
                //},
                //new Color
                //{
                //    BtnColor = System.Drawing.Color.FromArgb(255, 255, 0 ,255),
                //    Name = "Silver",
                //    Code = "SLV"
                //},
            };
            _currentDeviceInfo = new DeviceInfo();
            _currentDevices = new List<DeviceInfo>();
            _filePath = Directory.GetCurrentDirectory() + "/devices.csv";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Buttons = new List<ToggleSimpleButton>();
            foreach (var color in Colors)
            {
                var tsb = new ToggleSimpleButton
                {
                    Identifier = color.Code,
                    Title =
                    {
                        Text = color.Name,
                        BackColor = color.BtnColor
                    },
                    Width = 131,
                    Height = 44,
                    Margin = new Padding(5, 8, 0, 0)
                };

                tsb.Toggled += ColorSelected;

                Buttons.Add(tsb);
            }

            UpdateButtons();
            LoadCurrentImeis();
        }

        private void LoadCurrentImeis()
        {
            using (var r = File.OpenText(_filePath))
            {
                var line = r.ReadLine();
                while(line != null)
                {
                    if (string.IsNullOrEmpty(line)) return;

                    var data = line.Split('|');

                    var addingDeviceInfo = new DeviceInfo
                    {
                        Imei = data[0],
                        ProductType = data[1],
                        Capacity = data[2],
                        Color = data[3]
                    };

                    if(!_currentDevices.Any(cd => cd.Equals(addingDeviceInfo)))
                        _currentDevices.Add(addingDeviceInfo);

                    line = r.ReadLine();
                }
            }
        }

        private void ColorSelected(object sender, ToggledButtonToggledEventArgs args)
        {
            var tsb = (ToggleSimpleButton) sender;

            if (!tsb.IsPressed)
            {
                _currentDeviceInfo.Color = string.Empty;
                return;
            }

            foreach (var button in Buttons)
            {
                if(button != tsb)
                    button.SetPressed(false, false);
            }

            _currentDeviceInfo.Color = tsb.Identifier;
        }

        private void UpdateButtons()
        {
            Invoke(new Action(() =>
            {
                foreach (var b in Buttons)
                    flpButtonsContainer.Controls.Add(b);
            }));
        }

        private void btnReset_Click(object sender, EventArgs e)
            => Reset();

        private void Reset()
        {
            lbImeiValue.Text = @"-";
            lbProductTypeValue.Text = @"-";
            lbCapacityValue.Text = @"-";

            Buttons.ForEach(b => b.SetPressed(false, false));

            _currentDeviceInfo = new DeviceInfo();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            ReadDevice();
        }

        private void ReadDevice()
        {
            if (Reading) return;

            Reading = true;
            new Task(() =>
            {
                try
                {
                    _currentDeviceInfo.Imei = _reader.GetImei();
                    _currentDeviceInfo.ProductType = _reader.GetProductType();
                    _currentDeviceInfo.Capacity = _reader.GetCapacity() + @"GB";
                    ResetColorSelection();
                }
                catch (ErrorRetreivingDataFromLibException)
                {
                    ModalDialogBox.Show("Could not read the device.");
                }
                
                Invoke(new Action(() =>
                {
                    lbImeiValue.Text = _currentDeviceInfo.Imei;
                    lbProductTypeValue.Text = _currentDeviceInfo.ProductType;
                    lbCapacityValue.Text = _currentDeviceInfo.Capacity;
                }));
                Reading = false;
            }).Start();
        }

        private void ResetColorSelection()
        {
            _currentDeviceInfo.Color = string.Empty;
            Buttons.ForEach(b => b.SetPressed(false, false));
        }

        private void SetWaitingState(bool waiting)
        {
            Invoke(new Action(() =>
            {
                btnRead.Enabled = !waiting;
                btnStore.Enabled = !waiting;
                btnReset.Enabled = !waiting;
            }));
        }

        private void btnStore_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_currentDeviceInfo.Imei))
            {
                ModalDialogBox.Show("Please read the device.");
                return;
            }

            if (string.IsNullOrEmpty(_currentDeviceInfo.Color))
            {
                ModalDialogBox.Show("Please select the color.");
                return;
            }

            if (_currentDevices.Contains(_currentDeviceInfo))
            {
                //                _currentDevices.Remove(_currentDeviceInfo);
                //                _currentDevices.Add(_currentDeviceInfo);
                var i = _currentDevices.IndexOf(_currentDeviceInfo);
                _currentDevices[i] = _currentDeviceInfo;

                ReWriteFile();
                Reset();
                // ModalDialogBox.Show("Device is already in the file.");
                return;
            }

            StoreCurrentDevice();
            Reset();
        }

        private void StoreCurrentDevice()
        {
            using (var w = File.AppendText(_filePath))
            {
                w.WriteLine($"{_currentDeviceInfo.Imei}|{_currentDeviceInfo.ProductType}|{_currentDeviceInfo.Capacity}|{_currentDeviceInfo.Color}");
                _currentDevices.Add(_currentDeviceInfo);
            }
        }

        private void ReWriteFile()
        {
            File.WriteAllText(_filePath, string.Empty);

//            var currentDevicesReversedCopy = _currentDevices;
//            currentDevicesReversedCopy.Reverse();

            using (var w = File.AppendText(_filePath))
            {
                foreach (var d in _currentDevices)
                    w.WriteLine($"{d.Imei}|{d.ProductType}|{d.Capacity}|{d.Color}");
            }
        }
    }

    public class Color
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public System.Drawing.Color BtnColor { get; set; }
    }

    public class DeviceInfo
    {
        public string Imei { get; set; }
        public string ProductType { get; set; }
        public string Capacity { get; set; }
        public string Color { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is DeviceInfo di))
                return false;

            return di.Imei.Equals(Imei);
        }

        public override int GetHashCode()
        {
            var hash = 13;
            hash = hash * 7 + Imei.GetHashCode();
            hash = hash * 7 + ProductType.GetHashCode();
            hash = hash * 7 + Capacity.GetHashCode();
            hash = hash * 7 + Color.GetHashCode();
            return hash;
        }
    }
}