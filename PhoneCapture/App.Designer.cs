﻿namespace PhoneCapture
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbImei = new System.Windows.Forms.Label();
            this.lbProductType = new System.Windows.Forms.Label();
            this.lbCapacity = new System.Windows.Forms.Label();
            this.lbImeiValue = new System.Windows.Forms.Label();
            this.lbProductTypeValue = new System.Windows.Forms.Label();
            this.lbCapacityValue = new System.Windows.Forms.Label();
            this.flpButtonsContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.btnStore = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbImei
            // 
            this.lbImei.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lbImei.Location = new System.Drawing.Point(9, 9);
            this.lbImei.Margin = new System.Windows.Forms.Padding(0);
            this.lbImei.Name = "lbImei";
            this.lbImei.Size = new System.Drawing.Size(134, 24);
            this.lbImei.TabIndex = 0;
            this.lbImei.Text = "IMEI:";
            this.lbImei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbProductType
            // 
            this.lbProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lbProductType.Location = new System.Drawing.Point(9, 33);
            this.lbProductType.Margin = new System.Windows.Forms.Padding(0);
            this.lbProductType.Name = "lbProductType";
            this.lbProductType.Size = new System.Drawing.Size(134, 24);
            this.lbProductType.TabIndex = 1;
            this.lbProductType.Text = "PRODUCT TYPE:";
            this.lbProductType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCapacity
            // 
            this.lbCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lbCapacity.Location = new System.Drawing.Point(9, 57);
            this.lbCapacity.Margin = new System.Windows.Forms.Padding(0);
            this.lbCapacity.Name = "lbCapacity";
            this.lbCapacity.Size = new System.Drawing.Size(134, 24);
            this.lbCapacity.TabIndex = 2;
            this.lbCapacity.Text = "CAPACITY:";
            this.lbCapacity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbImeiValue
            // 
            this.lbImeiValue.AutoSize = true;
            this.lbImeiValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lbImeiValue.Location = new System.Drawing.Point(146, 14);
            this.lbImeiValue.Name = "lbImeiValue";
            this.lbImeiValue.Size = new System.Drawing.Size(11, 15);
            this.lbImeiValue.TabIndex = 3;
            this.lbImeiValue.Text = "-";
            // 
            // lbProductTypeValue
            // 
            this.lbProductTypeValue.AutoSize = true;
            this.lbProductTypeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbProductTypeValue.Location = new System.Drawing.Point(146, 38);
            this.lbProductTypeValue.Name = "lbProductTypeValue";
            this.lbProductTypeValue.Size = new System.Drawing.Size(11, 15);
            this.lbProductTypeValue.TabIndex = 4;
            this.lbProductTypeValue.Text = "-";
            // 
            // lbCapacityValue
            // 
            this.lbCapacityValue.AutoSize = true;
            this.lbCapacityValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbCapacityValue.Location = new System.Drawing.Point(146, 62);
            this.lbCapacityValue.Name = "lbCapacityValue";
            this.lbCapacityValue.Size = new System.Drawing.Size(11, 15);
            this.lbCapacityValue.TabIndex = 5;
            this.lbCapacityValue.Text = "-";
            // 
            // flpButtonsContainer
            // 
            this.flpButtonsContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.flpButtonsContainer.Location = new System.Drawing.Point(12, 84);
            this.flpButtonsContainer.Name = "flpButtonsContainer";
            this.flpButtonsContainer.Size = new System.Drawing.Size(413, 165);
            this.flpButtonsContainer.TabIndex = 6;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(294, 264);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(130, 36);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnRead
            // 
            this.btnRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnRead.Location = new System.Drawing.Point(12, 264);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(130, 36);
            this.btnRead.TabIndex = 8;
            this.btnRead.Text = "READ";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnStore
            // 
            this.btnStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnStore.Location = new System.Drawing.Point(153, 264);
            this.btnStore.Name = "btnStore";
            this.btnStore.Size = new System.Drawing.Size(130, 36);
            this.btnStore.TabIndex = 9;
            this.btnStore.Text = "STORE";
            this.btnStore.UseVisualStyleBackColor = true;
            this.btnStore.Click += new System.EventHandler(this.btnStore_Click);
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 312);
            this.Controls.Add(this.btnStore);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.flpButtonsContainer);
            this.Controls.Add(this.lbCapacityValue);
            this.Controls.Add(this.lbProductTypeValue);
            this.Controls.Add(this.lbImeiValue);
            this.Controls.Add(this.lbCapacity);
            this.Controls.Add(this.lbProductType);
            this.Controls.Add(this.lbImei);
            this.Name = "App";
            this.Text = "Capture Device";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbImei;
        private System.Windows.Forms.Label lbProductType;
        private System.Windows.Forms.Label lbCapacity;
        private System.Windows.Forms.Label lbImeiValue;
        private System.Windows.Forms.Label lbProductTypeValue;
        private System.Windows.Forms.Label lbCapacityValue;
        private System.Windows.Forms.FlowLayoutPanel flpButtonsContainer;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button btnStore;
    }
}

